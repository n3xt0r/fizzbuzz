<?php

namespace Example\Project\App;

class App
{

    protected ?self $application;

    /**
     * @return App|null
     */
    public function getApplication(): ?App
    {
        return $this->application;
    }

    /**
     * @param App|null $application
     */
    public function setApplication(?App $application): void
    {
        $this->application = $application;
    }

    public static function boot(): void
    {
        $self = new self();
        $self->init();
    }

    public function init(): void
    {
        if (null === $this->getApplication()) {
            $self = new self();
            $this->setApplication($self);
        }
    }
}