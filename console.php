<?php

use Example\Project\App\App;

const ROOT_PATH = __DIR__;
require ROOT_PATH . '/vendor/autoload.php';

App::boot();