FROM php:7.4-fpm
RUN apt-get update \
    && apt-get install -y wget curl libzip-dev unzip  \
    --no-install-recommends
RUN docker-php-ext-install zip \
    && pecl install xdebug \
    && docker-php-ext-enable xdebug \
    && curl -sS https://getcomposer.org/installer \
                 | php -- --install-dir=/usr/local/bin --filename=composer
WORKDIR /var/www/html